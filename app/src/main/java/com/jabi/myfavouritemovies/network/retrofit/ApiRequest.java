package com.jabi.myfavouritemovies.network.retrofit;

import com.jabi.myfavouritemovies.constants.AppConstants;
import com.jabi.myfavouritemovies.network.response.MovieListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiRequest extends AppConstants {

    @Headers({
               HeaderKey_ApiKey + sepatator + API_KEY ,
               HeaderKey_x_rapidapi_host + sepatator + HeaderValue_x_rapidapi_host
     })
    @GET(BASE_URL+fetchMoviesUrl)
    Call<MovieListResponse> getMovieArticles();
}
