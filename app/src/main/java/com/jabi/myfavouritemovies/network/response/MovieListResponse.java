package com.jabi.myfavouritemovies.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jabi.myfavouritemovies.model.Movie;

import java.util.List;

public class MovieListResponse {

//    @SerializedName("title")
//    @Expose
//    private String movieTitle ;
//
//    @SerializedName("image")
//    @Expose
//    private String movieUrl;
//
//    @SerializedName("id")
//    @Expose
//    private String movieId;

    @SerializedName("titles")
    @Expose
    private List<Movie> movies = null;

    public List<Movie> getMovies() {
        return movies;
    }
}
