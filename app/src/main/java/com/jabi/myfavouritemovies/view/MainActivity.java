package com.jabi.myfavouritemovies.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.jabi.myfavouritemovies.R;
import com.jabi.myfavouritemovies.adapter.MoviesListAdapter;
import com.jabi.myfavouritemovies.model.Movie;
import com.jabi.myfavouritemovies.roomDataBase.MovieDBModel;
import com.jabi.myfavouritemovies.viewModel.MovieListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView movieList;
    private ProgressBar progressBar;
    private LinearLayoutManager layoutManager;
    private MoviesListAdapter adapter;
    private ArrayList<Movie> movieDataList = new ArrayList<>();
    MovieListViewModel movieListViewModel;

    MovieDBModel movieDBModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        initialization();
        getMovies();

    }

    private void initialization() {
        movieList = (RecyclerView) findViewById(R.id.rv_movie_list);
         // use a linear layout manager
        layoutManager = new LinearLayoutManager(MainActivity.this);
        movieList.setLayoutManager(layoutManager);

        movieList.setHasFixedSize(true);

        // adapter
        adapter = new MoviesListAdapter(MainActivity.this, movieDataList);
        movieList.setAdapter(adapter);

        // View Model
        movieListViewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
    }

    private void getMovies() {
        if(movieListViewModel == null) return;

        movieListViewModel.getMovieListLiveData().observe(this,movieResponse -> {
            if (movieResponse != null) {
                List<Movie> movie = movieResponse.getMovies();
                movieDataList.addAll(movie);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private List<Movie> getStubs(){
        List<Movie> movieList = new ArrayList<>();
        for(int i = 0 ;  i < 10 ; i ++){
            Movie movie = new Movie();
            movie.setTitle("Movie");
            movieDataList.add(movie);
        }
        return movieList;
    }

}