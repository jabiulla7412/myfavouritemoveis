package com.jabi.myfavouritemovies.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jabi.myfavouritemovies.R;
import com.jabi.myfavouritemovies.model.Movie;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Movie> dataList;

    public MoviesListAdapter(Context context, ArrayList<Movie> dataList){
        this.context  = context ;
        this.dataList = dataList ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView movieName ;
        private TextView movieDesc ;
        private TextView favMovie  ;
        private ImageView movieImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            movieName = (TextView) itemView.findViewById(R.id.tv_movie_title);
            movieDesc = (TextView) itemView.findViewById(R.id.tv_movie_desc);
            favMovie  = (TextView) itemView.findViewById(R.id.tv_favourite);
            favMovie.setOnClickListener(this);
            movieImage = (ImageView) itemView.findViewById(R.id.iv_movie_image);
        }

        @Override
        public void onClick(View view) {

        }
    }

    @NonNull
    @Override
    public MoviesListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_movie_list,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesListAdapter.ViewHolder viewHolder, int position) {
        Movie movie = dataList.get(position);
        viewHolder.movieName.setText(movie.getTitle());
        Glide.with(context)
                .load(movie.getImageUrl())
                .into(viewHolder.movieImage);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
