package com.jabi.myfavouritemovies.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.jabi.myfavouritemovies.network.response.MovieListResponse;
import com.jabi.myfavouritemovies.repo.MoviesRepository;

public class MovieListViewModel extends AndroidViewModel {

    private MoviesRepository moviesRepository;
    private LiveData<MovieListResponse> moviesListResponseLiveData;

    public MovieListViewModel(@NonNull Application application) {
        super(application);
        moviesRepository = new MoviesRepository();
        this.moviesListResponseLiveData = moviesRepository.getMovieArticles();
    }

    public LiveData<MovieListResponse> getMovieListLiveData() {
        return moviesListResponseLiveData;
    }

}
