package com.jabi.myfavouritemovies.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.jabi.myfavouritemovies.roomDataBase.AppDatabase;
import com.jabi.myfavouritemovies.roomDataBase.MovieDBModel;

public class AddMovieDBViewModel extends AndroidViewModel {

    private AppDatabase appDatabase;

    public AddMovieDBViewModel(Application application) {
        super(application);

        appDatabase = AppDatabase.getAppDatabase(application);
    }

    public void addItem(MovieDBModel movieDBModel){
         appDatabase.getMoviesDao().addItem(movieDBModel);
    }
}
