package com.jabi.myfavouritemovies.roomDataBase;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MovieDBModel {

    @PrimaryKey(autoGenerate = true)
    private String id;

    private String movieName;
    private String movieDesc;
    private String movieUrl;

    public MovieDBModel(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDesc() {
        return movieDesc;
    }

    public void setMovieDesc(String movieDesc) {
        this.movieDesc = movieDesc;
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public void setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
    }
}
