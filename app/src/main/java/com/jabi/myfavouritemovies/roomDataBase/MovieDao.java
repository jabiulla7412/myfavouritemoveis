package com.jabi.myfavouritemovies.roomDataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MovieDao {

    @Query("Select * from MovieDBModel")
    LiveData<List<MovieDBModel>> getMovies();

    @Insert(onConflict = REPLACE)
    void addItem(MovieDBModel itemModel);

}
