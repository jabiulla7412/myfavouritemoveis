package com.jabi.myfavouritemovies.roomDataBase;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * Created by hirenpatel on 04/10/17.
 */

@Database(entities = {MovieDBModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "item_db").build();
        }
        return INSTANCE;
    }

    public abstract MovieDao getMoviesDao();

    public void destroyDB(){
        INSTANCE = null;
    }

}
