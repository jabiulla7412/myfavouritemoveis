package com.jabi.myfavouritemovies.repo;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jabi.myfavouritemovies.network.response.MovieListResponse;
import com.jabi.myfavouritemovies.network.retrofit.ApiRequest;
import com.jabi.myfavouritemovies.network.retrofit.MovieListRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesRepository {

    private static final String TAG = MoviesRepository.class.getSimpleName();
    private ApiRequest apiRequest;

    public MoviesRepository() {
        apiRequest = MovieListRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public LiveData<MovieListResponse> getMovieArticles() {

        final MutableLiveData<MovieListResponse> data = new MutableLiveData<>();

        apiRequest.getMovieArticles()
                .enqueue(new Callback<MovieListResponse>() {
                    @Override
                    public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                        Log.d(TAG, "onResponse response:: " + response);

                        if (response.body() != null) {
                            data.setValue(response.body());
                            Log.d(TAG, "onResponse: " + response.body() );

                        }
                    }

                    @Override
                    public void onFailure(Call<MovieListResponse> call, Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }
}
